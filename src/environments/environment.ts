// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase :{
    apiKey: "AIzaSyDQ6DNOTkmmUauFZKqSgJphyevmJgEpJ9g",
    authDomain: "testangular-35c94.firebaseapp.com",
    databaseURL: "https://testangular-35c94.firebaseio.com",
    projectId: "testangular-35c94",
    storageBucket: "testangular-35c94.appspot.com",
    messagingSenderId: "672862996267",
    appId: "1:672862996267:web:496a345d099ef78148a76e",
    measurementId: "G-8WS0WEMEPE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
