import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  email: string;

  constructor(public authService:AuthService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(user => {
      this.email = user.email;
    })
  }

}
