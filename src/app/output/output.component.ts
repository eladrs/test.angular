import { Component, OnInit } from '@angular/core';
import { ImageService } from '../services/image.service';
import { ClassificationService } from './../services/classification.service';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  constructor(public classificationService:ClassificationService,
              public imageService:ImageService) {}
         

  //רק בשלב הזה אנחנו בעצם מפעילים את הפונקציה ששולחת את המאמר לצורך הסיווג 
  ngOnInit() {
    this.classificationService.classify().subscribe(
      res => {
        this.category = this.classificationService.categories[res];//פה אני בעצם מבקש לקבל את הסיווג דרך קובץ הסרוויס
        this.categoryImage = this.imageService.images[res];
      }
    )
  }
}
