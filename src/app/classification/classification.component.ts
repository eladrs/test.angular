import { Router } from '@angular/router';
import {ClassificationService } from './../services/classification.service';
import { Component} from '@angular/core';


@Component({
  selector: 'app-classification',
  templateUrl: './classification.component.html',
  styleUrls: ['./classification.component.css']
})
export class ClassificationComponent {

  constructor(private classificationService:ClassificationService,
  private router:Router) { }

text:string;

onSubmit(){
  this.classificationService.doc=this.text;
  this.router.navigate(['/output'])
}

}