import { Comment } from './../interfaces/users';
import { User } from './../interfaces/user.model';
import { Post } from './../interfaces/posts';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';//Observable שזו הספרייה של  rxjs מספריית  map יבוא של האופרטור 
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private usersCollection:AngularFirestoreCollection<User>;//יצירת משתנה מסוג שיאפשר גישה לקולקשיין בתוך הדאטה בייס 
  private users:Observable<User[]>;//get בשביל להשתמש אחרי זה בפונקציות  Item מסוג  Observable יצירת משתנה שיוכל לקבל ערכי 
  private postsCollection:AngularFirestoreCollection;
  private posts: Observable<Post[]>;

private POSTURL = "https://jsonplaceholder.typicode.com/posts/";// (JSONPlaceholder) הנתון  api כאן אנו משתמשים ב 
private COMMENTRURL = "https://jsonplaceholder.typicode.com/comments";
private SINGlE = "https://jsonplaceholder.typicode.com/posts/";


constructor(private http: HttpClient,private db:AngularFirestore){
  this.usersCollection=db.collection<User>('users');
}
    //כאשר בדרך היא אוספת נתונים נוספים דרך הפונקציה שמוסיפה תגובות API שיטה שמטרתה להביא את הנתונים דרך ה 
    getData(): Observable<Post[]>{
    this.posts = this.http.get<Post[]>(`${this.POSTURL}`)
    .pipe(
     map((data) => this.addCommentsToPosts(data)));
     return this.posts;    
    }

    //פה אני מקבל את הנתונים עבור התגובות של הפוסטים
    getComment():Observable<Comment[]>{
    const comments = this.http.get<Comment[]>(`${this.COMMENTRURL}`);
    console.log(comments)
    return comments;
    }

    //עבור הוספת פוסטים לדאטאבייס
    addPost(title:string,body:string,userId:string){
      console.log("i'm added post", userId )
      const postAdded={title:title,body:body}
      this.usersCollection.doc(userId).collection('posts').add(postAdded);
    }

    //בכדי להציג את הערכים ששמורים בדאטא בייס עבור היוזר ששמר אותם-זה שמחובר כרגע
    getItems(userId:string):Observable<any[]>{
      this.postsCollection=this.db.collection(`users/${userId}/posts`)
      console.log('items collection created');
      return this.postsCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data();
          data.id = a.payload.doc.id;
          return { ...data };
        }))
      );
    }

    getSinglePost(id:number): Observable<Post>{
      return this.http.get<Post>(`${this.SINGlE}${id}`);
    }

    

  
    // getPosts():Observable<Post[]>{//doc של כל  id בשיטה הזאת אני יכול לקבל את ה 
    // this.posts=this.itemsCollection.valueChanges({idField:'id'});//doc של כל  id פה אני בעצם מקבל את ה 
    // return this.posts; 
    // } 

    //שיטה לראות את הנתונים שמופיעים בדאטאבייס
    getPost(id:string):Observable<any>{
      return this.db.doc(`posts/${id}`).get();
    }
    // //הוספת פוסט לדאטא בייס          
    // addPost(title:string,body:string, author:string){
    //  const post= {author:author,title:title,body:body}
    //  this.db.collection('posts').add(post);
    // }
    
    //עריכה
    updatePost(id:string,title:string,body:string,author:string){
      this.db.doc(`posts/${id}`).update(
      {
      title:title,
      body:body,
      author:author
      })
       } 
       
    //מחיקה
    deletePost(id:string){
    console.log("This is the postID: "+id)
     this.db.doc(`posts/${id}`).delete();
    }   

//בשיטה הזאת אני מוסיף את התגובה על הפוסט לתוך מערך של פוסטים
 addCommentsToPosts(data: Post[]): Post[]{
   const users = this.getComment();
   const postsArray = []
   users.forEach(comment => {    
    comment.forEach(c =>{
           data.forEach(post =>{
               if(post.id === c.postId){
                   postsArray.push({
                       id: post.id,
                       userId:post.userId,
                       title: post.title,
                       body: post.body,
                       postBody:c.body
                   })
               }
           })
       })
   })
 return postsArray;
 }
}