import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ClassificationService {
  
  private url = "https://o60mxx7cql.execute-api.us-east-1.amazonaws.com/beta"
  public categories:object ={0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  public doc :string;

  constructor(private http:HttpClient) { }
  
  classify():Observable<any>{
    let json={
      "articles":[
        {"text":this.doc}
      ]
    }
    let body= JSON.stringify(json);//פעולה שהופכת את התוכן לסטרינג/טקסט
    return  this.http.post<any>(this.url,body).pipe(
      map(res=>{
        console.log(res.body)
        let final = res.body.replace('[','')//במבנה שאני מקבל אני מקבל מערך כלומר יהיה לי סוגריים מרובעות שאותם ארצה להוריד וכך אעשה זאת
        final = final.replace(']','');
        return final;
      })
    )
  }
}