//import { User } from './../../interfaces/user.model';
import { Auth } from '../interfaces/auth';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component } from '@angular/core';
import { NgForm} from '@angular/forms'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {
isLoginMode=false;
error:string=null;

constructor(private authService :AuthService,private router:Router) { }
email:string;
password:string;

onSwitchMode(){
    this.isLoginMode=!this.isLoginMode 
}

onSubmit(form:NgForm){
    if(!form.valid){return;}//אם המשתמש הצליח לעקוף את חוקי הולידציה אז הוספנו פה חוק נוסף שנועד למנוע שליחת טופס שגוי
    console.log("לחצתי על הכפתור")
    const email = form.value.email;
    const password = form.value.password;
    let authObs: Observable <Auth>
    if(this.isLoginMode){
      authObs=this.authService.login(email,password);
    }else{
      authObs=this.authService.signup(email,password);
    }

    authObs.subscribe(
      resData=>{
        console.log(resData)
        this.router.navigate(['/welcome']);
      },
      errorMessage=>{
        console.log(errorMessage);  
        this.error= errorMessage;
      }
    );

    form.reset();//כדי שהשדות ישארו ריקים לאחר שליחת הטופס 
  }
}


