import { PostsService } from './../services/posts.service';
import { Post } from './../interfaces/posts';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';
import { AuthService } from './../services/auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})

  export class BlogPostsComponent implements OnInit{
    panelOpenState = false;
    title:string;
    body:string;
    userId:string;
    posts$: Observable<Post[]>;
    postblog$: Observable<Post>;
   
    
    constructor(private postsService:PostsService,private router: Router,public authService:AuthService) { }
    likes:number=0;

    addLikes(){
      this.likes++
    }
  
    ngOnInit() {
      this.posts$= this.postsService.getData();
      // this.authService.getUser();
      this.authService.getUser().subscribe(u =>{
        console.log(u.id)
        this.userId = u.id
        })
    }
    save(id: number, uid){
      this.postblog$ = this.postsService.getSinglePost(id);
      this.postblog$.subscribe(post => {
        this.postsService.addPost(post.title,post.body, uid);
        // (title:string,body:string,userId:string)
      })
      // this.message = "The post added successfully!";
      // this.notificationService.success(this.message);
    }
  
    // addPostsToFirestore(){
    //   this.postsService.addToFirestore();
    //   this.router.navigate(['/posts']);
    // }
   }
  